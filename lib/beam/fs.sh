#! /bin/sh
# This file is part of BEAM
# Copyright (C) 2012-2014 Sergey Poznyakoff
#
# BEAM is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# BEAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BEAM.  If not, see <http://www.gnu.org/licenses/>.

# Configuration keywords:
#
# item_type=fs                  [mandatory]
# item_dir=STRING               [mandatory]
# item_files=STRING             [mandatory]
# item_exclude=STRING           [optional]
# item_exclude_from=STRING      [optional]
# item_tar_options=STRING       [optional internal]
#

# initdb item
# Initializes snapshot for the given basename.
initdb() {
    local filename
    
    if [ -n "$dry_run" ]; then
	logit "initializing snapshot for $1"
	return
    fi

    if [ $level -eq 0 ]; then
	filename=$backup_snapshot_dir/$1-$week-$round-$level.db
	test -r $filename && rm $filename
    else 
	if [ $level -eq 1 ]; then
	    filename=$backup_snapshot_dir/$1-$week-0-0.db
	else
	    filename=$backup_snapshot_dir/$1-$week-$round-$((level - 1)).db
	fi
	if [ -r $filename ]; then
	    cp $filename $backup_snapshot_dir/$1-$week-$round-$level.db
	else
	    logit "previous snapshot file $filename not found; falling back to level 0"
	    filename=$backup_snapshot_dir/$1-$week-$round-$level.db
	    test -r $filename && rm $filename
	fi
    fi
}

# fs_check item
fs_check() {
    local rc=0 root files

    eval root=\$${1}_dir
    eval files=\$${1}_files

    test -z "$root" && rc=1 && error "${1}_dir not set" 
    test -z "$files" && rc=1 && error "${1}_files not set"
    return $rc
}

# fs_list item prefix
fs_list() {
    local dir files lsf=
  
    eval dir=\$${1}_dir files=\$${1}_files
    for file in $files
    do
	if [ -d "$dir/$file" ]; then
	    lsf="$lsf $file/"
	else
	    lsf="$lsf $file"
	fi
    done
    echo "${2}Files and directories in $dir:$lsf"
}

# fs_backup item
fs_backup() {
    local basename text root files exclude addopts s e

    basename=$1-$week-$round-$level
    eval text=\$${1}_text
    eval root=\$${1}_dir
    eval files=\$${1}_files
    eval addopts=\$${1}_tar_options
    
    eval exclude=\$${1}_exclude
    for e in $exclude
    do
	eval s="$e"
	addopts="$addopts --exclude=$s"
    done
    
    eval exclude=\$${1}_exclude_from
    for e in $exclude
    do
	addopts="$addopts --exclude-from=$e"
    done
    
    test -z "$root" && abend 1 "${1}_dir not set"
    test -z "$files" && abend 1 "${1}_files not set"
    test -z "$text" && text="$1"
    initdb $1
    logit "backing up $text ($basename.$tar_suffix)"
    $dry_run tar $verbose $taroptions $addopts \
	-f $backup_archive_dir/$basename.$tar_suffix \
	--listed=$backup_snapshot_dir/$basename.db \
	-C $root $files
    tarcode $?
}

# fs_restore item
fs_restore() {
    local i text root files tarcommand

    eval text=\$${1}_text
    eval root=\$${1}_dir
    eval files=\$${1}_files
    
    test -z "$root" && abend 1 "${1}_dir not set"
    test -z "$files" && abend 1 "${1}_files not set"
    test -z "$text" && text="$1"
    
    tarcommand="tar $verbose $taroptions -C $root --listed-incremental=/dev/null -f"
  
    logit "restoring $text"
    logit "restoring from level 0 backup"

    $dry_run $tarcommand $backup_archive_dir/$1-$week-0-0.$tar_suffix
    tarcode $?

    for i in $(seq 1 $level)
    do
	logit "restoring from the $round/$i backup"  
	$dry_run $tarcommand $backup_archive_dir/$1-$week-$round-$i.$tar_suffix
	tarcode $?
    done
    logit "finished restoring $text"
}

