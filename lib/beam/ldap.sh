#! /bin/sh
# This file is part of BEAM
# Copyright (C) 2012-2014 Sergey Poznyakoff
#
# BEAM is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# BEAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BEAM.  If not, see <http://www.gnu.org/licenses/>.

# Configuration keywords:
#
# item_type=ldap                 [mandatory]
# item_database_directory=string [mandatory] 
# item_database_number=n         [optional *]
# item_database_suffix=suf       [optional *]
# item_uri=str                   [optional]
# item_user=name                 [optional]
# item_slapcat_options=string    [optional internal]
# item_slapadd_options=string    [optional internal]
# 
# * - mutually exclusive keywords.


# ldap_check item
ldap_check() {
    local rc=0 dbno dbsuf dbdir
    eval dbno=\$${1}_database_number \
         dbsuf=\$${1}_database_suffix \
         dbdir=\$${1}_database_directory
    if test -n "$dbno" && test -n "$dbsuf"; then 
        error "$1: both ${1}_database_number and ${1}_database_suffix are set"
        rc=1 
    fi
    if test -z "$dbdir"; then
	error "${1}_database_directory not configured"
	rc=1
    fi
    if ! test -d "$dbdir"; then
	error "LDAP database directory $dbdir does not exist"
	error "(set by ${1}_database_directory)"
    fi
    return $rc
}

# ldap_list item prefix
ldap_list() {
    local dbid
    
    eval dbid=\$${1}_database_number
    if [ -n "$dbid" ]; then
	dbid=" number $dbid"
    else
        eval dbid=\$${1}_database_suffix
        if [ -n "$dbid" ]; then
	    dbid=" suffixed with $dbid"
	fi
    fi

    echo "${2}LDAP database"$dbid
}

# ldap_backup item
ldap_backup() {
    local options dbno dbsuf uri

    logit "backing up LDAP database $1"
    
    eval options=\$${1}_slapcat_options \
         dbno=\$${1}_database_number \
         dbsuf=\$${1}_database_suffix

    if [ -n "$dbno" ]; then
	options="$options -n$dbnum"
    elif [ -n "$dbsuf" ]; then
	options="$options -b$dbsuf"
    fi

    eval uri=\"\$${1}_uri\"
    if [ -n "$uri" ]; then
	options="$options -H\"$uri\""
    fi

    if [ -z "$dry_run" ]; then
	slapcat $options > $backup_snapshot_dir/$1-$week-$round-$level
    else
	echo "slapcat $options > $backup_snapshot_dir/$1-$week-$round-$level"
    fi

    if [ $? -ne 0 ]; then
	tarerror=$((tarerror + 1))
        logit "failed"
    else
        logit "creating $1-$week-$round-$level.$tar_suffix"
        $dry_run tar $verbose $taroptions \
                -f $backup_archive_dir/$1-$week-$round-$level.$tar_suffix \
                -C $backup_snapshot_dir $1-$week-$round-$level
        tarcode $?
        $dry_run rm $backup_snapshot_dir/$1-$week-$round-$level
    fi
}

# ldap_restore item
ldap_restore() {
    local options dbno dbsuf dbdir u user
    
    logit "extracting LDAP database dump $1"
    
    eval options=\$${1}_slapadd_options dbno=\$${1}_database_number \
         dbsuf=\$${1}_database_suffix dbdir=\$${1}_database_directory \
         user=\$${1}_user

    if test -z "$dbdir"; then
	error "${1}_database_directory not configured"
	tarerror=$((tarerror + 1))
	return
    fi
    if [ -n "$dbno" ]; then
	options="$options -n$dbnum"
    elif [ -n "$dbsuf" ]; then
	options="$options -b$slapcat"
    fi
  
    u=$(umask)
    trap "umask $u" 1 2 3 13 15
    umask 077
    $dry_run tar $verbose $taroptions \
           -f $backup_archive_dir/$1-$week-$round-$level.$tar_suffix
    e=$?
    tarcode $e
    if [ $e -eq 0 ]; then
	prevdbtar=$backup_snapshot_dir/ldap-$1-$(date +%Y%m%dT%H%M%S).tar
	logit "archiving the previous database contents to $prevdbtar"
	$dry_run tar $verbose -c -f $prevdbtar $dbdir
	e=$?
	tarcode $e
	umask $u
	if [ $e -ne 0 ]; then
	    error "Failed to backup the prior database contents to $prevdbtar"
	    return
        fi
	logit "removing old database files in $dbdir"
	$dry_run rm $dbdir/*
        logit "restoring database $1 from the dump"
        if test -n "$user"; then
            su $user -c "$dry_run slapadd $options $verbose -l $1-$week-$round-$level"
	    su $user -c "$dry_run slapindex"
        else
            $dry_run slapadd $options $verbose -l $1-$week-$round-$level
	    $dry_run slapindex
        fi 
        # FIXME: error checking
        $dry_run rm $1-$week-$round-$level  
    fi
    umask $u
    trap - 1 2 3 13 15
}
