#! /bin/sh
# This file is part of BEAM
# Copyright (C) 2012-2014 Sergey Poznyakoff
#
# BEAM is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# BEAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BEAM.  If not, see <http://www.gnu.org/licenses/>.

# Configuration keywords:
#
# item_type=mysql                               [mandatory]
# item_database=STRING                          [optional]
# item_defaults_file=STRING                     [optional]
# item_alldb=single|monolithic|split|individual [optional]
#

# mysql_check item
mysql_check() {
    return 0
}

# mysql_list item prefix
mysql_list() {
    local database

    eval database=\$${1}_database
    if [ -z "$database" ]; then
	echo "${2}all MySQL databases"
    else
        echo "${2}MySQL database $database"
    fi
}

# dump_mysql_db cmd stem
dump_mysql_db() {
    if [ -z "$dry_run" ]; then
	$1 $db > $backup_tmp_dir/$2-$week-$round-$level
    else
        echo "$1 $db > $backup_tmp_dir/$2-$week-$round-$level"
    fi

    if [ $? -ne 0 ]; then
	tarerror=$((tarerror + 1))
        logit "failed"
    else
	logit "creating $2-$week-$round-$level.$tar_suffix"
	$dry_run tar $verbose $taroptions \
	      -f $backup_archive_dir/$2-$week-$round-$level.$tar_suffix \
              -C $backup_tmp_dir $2-$week-$round-$level
        tarcode $?
        $dry_run rm $backup_tmp_dir/$2-$week-$round-$level
    fi
}    

restore_mysql_db() {
    local u
    
    logit "restoring MySQL database $1"
    u=$(umask)
    trap "umask $u" 1 2 3 13 15
    umask 077
    $dry_run tar $verbose $taroptions \
           -f $backup_archive_dir/$1-$week-$round-$level.$tar_suffix
    e=$?
    tarcode $e
    if [ $e -eq 0 ]; then
	logit "restoring database from the dump"
        # Stupid lossage: cannot give -A option here, because with it mysql
        # refuses to understand defaults-file.
        cmd="mysql"
        eval defaults_file=\$${1}_defaults_file
        if [ -n "$defaults_file" ]; then
	    cmd="$cmd --defaults-file=$defaults_file"
        fi
        if [ -n "$dry_run" ]; then
	    echo "$cmd < $1-$week-$round-$level"
        elif [ -r $1-$week-$round-$level ]; then
	    $cmd < $1-$week-$round-$level > db-$1.log
            if grep ERROR db-$1.log >/dev/null; then
		error "errors occurred during restore; see db-$1.log for details"
	        error "dump preserved in file $1-$week-$round-$level"
	        tarerror=$((tarerror + 1))
            else
                rm $1-$week-$round-$level
            fi
        fi
    fi
    umask $u
    trap - 1 2 3 13 15
}

# mysql_backup item
mysql_backup() {
    local creds cmd database alldb db stem

    eval database=\$${1}_database
    if [ -z "$database" ]; then
	logit "backing up all MySQL databases"
    else
	logit "backing up MySQL database $database"
    fi
    cmd="mysqldump"
    eval defaults_file=\$${1}_defaults_file
    if [ -n "$defaults_file" ]; then
	creds="--defaults-file=$defaults_file"
	cmd="$cmd $creds"
    fi
    cmd="$cmd --add-drop-database"
    cmd="$cmd --single-transaction"
    
    if test -z "$database"; then
	eval alldb=\$${1}_alldb
	case $alldb in
	single|monolithic)
	    cmd="$cmd --all-databases";;
	split|individual)
	    alldb=split
            cmd="$cmd --databases"
	    database=$(mysql $creds -e "show databases" -B --skip-column-names);;
        "") cmd="$cmd --all-databases";;
	*)  error "unknown value: ${1}_alldb=$alldb, assuming 'single'"
	    cmd="$cmd --all-databases";;
	esac	    
    else
	cmd="$cmd --databases"
    fi

    if [ -z "$database" ]; then
        dump_mysql_db "$cmd" "$1"
    else
	for db in $database
        do
	    if [ "$alldb" = split ]; then
		stem=$1-$db
	        logit "dumping $db"
	    else
	        stem=$1
	    fi
            dump_mysql_db "$cmd" "$stem"
        done
    fi
}

mysql_restore() {
    local database dbname remote

    eval database=\$${1}_database
    if test -z "$database"; then
	eval alldb=\$${1}_alldb
	case $alldb in
	split|individual)
		beam_exec find $backup_local_archive_dir \
		       -name $1-'*'-$week-$round-$level.$tar_suffix \
		       -printf '%f\\n' | \
		while read file
		do
		    restore_mysql_db $(expr "$file" : "$1-\(.[^-]*\)-.*")
                done		    
                ;;
        *)      restore_mysql_db $1
	esac	    
    else
	restore_mysql_db $database
    fi
}

