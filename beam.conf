# -*- shell-script -*-
# This is a sample config file for the backup system. Tailor it to your
# needs and remove the line below:
echo >&2 "$0: WARNING: using default configuration boilerplate"

# This file is essentially a shell script defining variables used by
# the backup scripts. For a detailed information about it, see beam.conf(5)
# To debug the configuration, run "beam backup --dry-run" and
# "beam restore --dry-run".
# For a detailed descriptions of these commands, see beam(1).

PATH=/usr/sbin:$PATH
export PATH

##########################################################################
# Hooks
##########################################################################
#
# Special variables, called hooks, allow you to supply arbitrary
# procedures to be run before and after backup.
# When set, a hook must contain a whitespace-separated list of commands to
# be invoked.  These commands will be invoked without arguments and in
# order of their appearance in the list.

# Commands listed in openlog_hook are run before opening the logfile.  The
# most common use for this hook is to rotate an oversized logfile prior to
# opening it.
#
# Beam's default hook is beam_logrotate, which uses logrotate(8) to manage
# the log file.
openlog_hook=beam_logrotate
# Additional configuration for logrotate can be supplied in the
# backup_logrotate_conf variable.
backup_logrotate_conf="weekly
rotate 4
"
# The prologue_hook is a list of commands to run before starting backup
# (or restore).  
prologue_hook=
# The epilogue_hook is a list of commands to run when the backup
# (or restore) finishes.
epilogue_hook=

##########################################################################
# Archivation setup
##########################################################################

# Interval in weeks during which old backups and snapshots are retained.
# If zero or empty, backups are retained forever.
backup_retain_interval=

# Directory for temporary files
backup_tmp_dir=/tmp

##########################################################################
# S3 configuration.
##########################################################################

# To use S3 as the backup storage, define this variable to the name of the
# bucket to keep backups in.
backup_bucket_name=

# Mountpoint for the raw bucket.
backup_mp_s3backer=/mnt/s3backer
# Mountpoint for the actual file system.
backup_mp_s3=/mnt/s3

# Additional options to pass to s3backer.  The use of --vhost option
# is advised if your bucket is located outside of the US region.
backup_s3backer_options="--vhost"

# Additional options to pass to mount(8)
backup_s3_mount_options=

##########################################################################
# Tar setup.
# 
# Tar setup variables configure invocation of tar.
#
##########################################################################

# Any additional options to pass to tar. Do not place tar operation
# switches (as -c, -t, etc.) here! These will be added automatically
# by appropriate scripts, depending on the operation being performed.
#
# By default this variable is empty (no additional options).
#
# In this example it is used to request bzip2 compression:
backup_tar_options="-j"

# Suffix for archive files.
# Default is "tar"
#
backup_suffix="tar.bz2"

# Directory where archive files are to be located. It's OK to specify
# a remote directory here, e.g. 10.10.0.1:/export/backup
#
# This variable must be set. Whatever directory it points to must already
# exist, the backup script won't create it.
#
# The only exception to this is when backup_bucket_name is defined.  In
# that case, if backup_archive_dir is empty, it is assigned to the value
# of backup_mp_s3 variable.  This directory will be created, if needed.
backup_archive_dir=/var/backups

# Directory where to store snapshot files. The files will be named as
# their archive counterparts, with the suffix ".db".
#
# This variable must be set
backup_snapshot_dir=/var/lib/backups

# Set this variable to a non-empty value if you wish backup procedures to
# be verbose.
backup_verbose=

##########################################################################
# Backup items.
#
# Backup items are symbolic names that identify abstract objects that
# need to be backed up (or restored). These must be valid shell variable
# names. For each backup item <name>, this configuration file defines a
# set of variables which determine what files to backup and what methods
# to use for that. Each such variable is named <name>_<variable>, where
# <name> is the item name, and <variable> is the variable name. The set
# of variables needed depends on the type of each particular item.
#
##########################################################################

# The following are sample entries. Be sure to edit them

# Define the list of items to back up.
# This variable must be set.
backup_items="dbdump system"

# For each item in $backup_items, at least its type (variable <item>_type)
# must be declared. Item type determines the mechanism used to back up
# and restore it. Valid item types are:
#
#  fs               back up a file system
#  postgres         back up a postgres database
#  mysql            back up a mysql database
#  ldap             back up an LDAP database
#
# You may define additional types, if you need. To do so, create an executable
# file named <type>.sh in the directory @LIBDIR@/beam.
# 

# Dump Postgres database "mydb".
# The <item>_database is the only variable required by type "postgres".
dbdump_type=postgres
dbdump_database="mydb"

# Back up directories in the file system.
# The "fs" type requires at least to variables:
#
#  <item>_dir specifies the directory (usually, but not necessarily a mount
#  point), under which the directories and files to be backed up are located.
#
#  <item>_files specifies a list of directories and/or files in that directory.

system_type=fs
system_dir="/"
system_files="etc var/spool"


##########################################################################
# Mail notification setup.
#
# Apart from usual logging, backup reports can be sent via email.  To
# enable this, define the variable "backup_notify_email" to the list
# of emails that should receive the reports.  Separate multiple addresses
# with commas.
#
# A set of variables is provided to customize report headers and contents.
##########################################################################

# Comma-separated list of emails to send backup reports to.  If emply,
# mail notifications are not sent.
backup_notify_email=

# Sender email address.  Backup reports will appear to be sent from this
# address.  The default value is root@$(hostname).
# If set, this variable must contain a single email address, without
# personal part or comments, e.g.
#  backup_sender_email=root@example.com
backup_sender_email=

# You can supply personal part of the sender email using this variable.
# The personal part will be enclosed in double quotes and prepended to
# the value of $backup_sender_email to form a valid RFC-2822 "From" header.
# For example, if you have:
#  backup_sender_email=root@example.com
#  backup_sender_personal="Automatic backup report"
# you will see the following in the report headers:
#  From: "Automatic backup report" <root@example.com>
backup_sender_personal=

# Supply any additional headers for the report message.  By default, the
# following headers are generated:
#  From, To, Subject, X-Beam-Items, X-Beam-Round and X-Beam-Level.
# The three X- headers contain the backed up items (as set in the
# backup_items variable), backup round and backup level numbers,
# correspondingly.
#
# To supply multiple headers, delimit them with a single newline character.
backup_report_headers=

# The value of this variable is output before the actual report.
backup_report_intro=

# The value of this variable is added at the end of the report.
backup_report_signature=

# Reports are sent using this program.  If not set, it defaults to
# /usr/sbin/sendmail -oi -t -F $backup_sender_email
# The generated report is piped to the standard input of this program.
backup_mailer_program=

